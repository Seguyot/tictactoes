class tictactoes {
    constructor() {
        this.playerNow = 0;
        this.confirmReload = false;
        this.timerOn = false;
        this.seconds = 0;
        this.minutes = 0;
        //variables pour le compteur
        this.nbHit = 0;

    }

    start() {
        this.timer(this.seconds, this.minutes);
        for (this.i = 1; this.i <= 9; this.i++) {
            let game = document.querySelector("#game");
            let caseTic = document.createElement("div");
            caseTic.classList.add("col-4");
            caseTic.id = "case" + this.i;
            caseTic.setAttribute("onclick", "game.tic(" + this.i + ")");

            game.appendChild(caseTic);      

        }

    }
    startCounter() {
        this.moves++;
        this.counter.innerHTML = this.moves;

        if(this.moves == 1){
            this.startTimer();
        }
    }
    tic(id) {
        let caseToTic = document.querySelector("#case" + id);
        let ShowNbHit = document.querySelector('#nbHit');
        if (caseToTic.classList.contains("x") || caseToTic.classList.contains("o")) {
            alert('you can\'play play here!');
        } else {
            
            if (this.playerNow == 0) {
                this.nbHit++;
                ShowNbHit.innerHTML = this.nbHit;
                caseToTic.classList.add('x');
                this.playerNow = 1;
                this.checkForWin();
                
            } else {
                this.nbHit++;
                ShowNbHit.innerHTML = this.nbHit;
                caseToTic.classList.add('o');
                this.playerNow = 0;
                this.checkForWin();
            }
        }
    }
    checkForWin() {
        let case1 = document.querySelector("#case1");
        let case2 = document.querySelector("#case2");
        let case3 = document.querySelector("#case3");
        let case4 = document.querySelector("#case4");
        let case5 = document.querySelector("#case5");
        let case6 = document.querySelector("#case6");
        let case7 = document.querySelector("#case7");
        let case8 = document.querySelector("#case8");
        let case9 = document.querySelector("#case9");

        if ((case1.classList.contains("x") && case2.classList.contains("x") && case3.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case4.classList.contains("x") && case5.classList.contains("x") && case6.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case7.classList.contains("x") && case8.classList.contains("x") && case9.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case1.classList.contains("x") && case5.classList.contains("x") && case9.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case3.classList.contains("x") && case5.classList.contains("x") && case7.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case1.classList.contains("x") && case4.classList.contains("x") && case7.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case2.classList.contains("x") && case5.classList.contains("x") && case8.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case3.classList.contains("x") && case6.classList.contains("x") && case9.classList.contains("x")) == true) {

            this.confirmReload = confirm('Bravo, player X won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);

            }

        }
        if ((case1.classList.contains("o") && case2.classList.contains("o") && case3.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?'); x
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        }else if ((case4.classList.contains("o") && case5.classList.contains("o") && case6.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case7.classList.contains("o") && case8.classList.contains("o") && case9.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case1.classList.contains("o") && case5.classList.contains("o") && case9.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case3.classList.contains("o") && case5.classList.contains("o") && case7.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case1.classList.contains("o") && case4.classList.contains("o") && case7.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            }
        } else if ((case2.classList.contains("o") && case5.classList.contains("o") && case8.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);
            } xx
        } else if ((case3.classList.contains("o") && case6.classList.contains("o") && case9.classList.contains("o")) == true) {

            this.confirmReload = confirm('Bravo, player O won!, \n \n Play Again?');
            if (this.confirmReload === true) {
                document.location.reload(true);


            }
        }
    }
    timer(seconds, minutes){
       
			setInterval(function(){
				console.log(minutes + " : " + seconds);
				seconds++;
				let showTimer = document.querySelector("#timer");
				showTimer.innerHTML = minutes + " : " + seconds;
				if (seconds == 60){
					minutes++;
					seconds=0;
				}
			}, 1000);
		}
    
}


    window.onload = function () {
        game = new tictactoes;
        game.start();
    };
